# Generated by Django 4.2.1 on 2023-06-01 21:15

from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        (
            "receipts",
            "0002_receipt_account_receipt_tax_alter_account_number_and_more",
        ),
    ]

    operations = [
        migrations.AlterField(
            model_name="receipt",
            name="date",
            field=models.DateTimeField(),
        ),
    ]
