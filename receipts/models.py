from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class ExpenseCategory(models.Model):
    # This defines the fields of your model
    name = models.CharField(max_length=50)
    owner = models.ForeignKey(
        User,
        related_name="categories",
        on_delete=models.CASCADE,
    )


    # This tells Django how to convert our model into a string
    # when we print() it, or when the admin displays it.
    def __str__(self):
        return self.name

class Account(models.Model):
    # This defines the fields of your model
    name = models.CharField(max_length=100)
    number = models.CharField(max_length=20)
    owner = models.ForeignKey(
        User,
        related_name="accounts",
        on_delete=models.CASCADE,
    )


class Receipt(models.Model):
    # This defines the fields of your model
    vendor = models.CharField(max_length=200)
    total = models.DecimalField(max_digits=10, decimal_places=3)
    tax = models.DecimalField(max_digits=10, decimal_places=3, default=0)
    date = models.DateTimeField()
    purchaser = models.ForeignKey(
        User,
        related_name="receipts",
        on_delete=models.CASCADE,
    )
    category = models.ForeignKey(
        ExpenseCategory,
        related_name="receipts",
        on_delete=models.CASCADE,
        default=0
    )
    account = models.ForeignKey(
        Account,
        related_name="receipts",
        on_delete=models.CASCADE,
        default=0
    )
